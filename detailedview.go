package main

import (
	"bitbucket.org/gmelchett/textcolor"
	"fmt"
	_ "github.com/davecgh/go-spew/spew"
	"strings"
	"time"
)

type WeekView struct {
	width int
}

func center(text string, size int) string {
	if len(text) > size {
		return text[:size-1] + "▶"
	}
	padding := size - len(text)
	startpadding := padding / 2
	endpadding := startpadding
	if padding&1 == 1 {
		endpadding++
	}
	return strings.Repeat(" ", startpadding) + text + strings.Repeat(" ", endpadding)
}

func left(text string, size int) string {
	if len(text) > size {
		return text[:size-1] + "▶"
	}
	return text + strings.Repeat(" ", size-len(text))
}

func (kalle *Kalle) showTime(hour int, showHour bool) string {
	m := textcolor.Bg(kalle.colorTheme.timeBg)
	if showHour && (hour+kalle.settings.DayStartHour) == kalle.now.Hour() {
		m += textcolor.Fg(kalle.colorTheme.currentHour)
	} else {
		m += textcolor.Fg(kalle.colorTheme.time)
	}

	if showHour {
		m += fmt.Sprintf("%02d:00", kalle.settings.DayStartHour+hour)
	} else {
		m += strings.Repeat(" ", 5)
	}
	return m
}

func (kalle *Kalle) prepareForDisplay(events *calenderCellEvents, resolution, numDays, dayWidth int) [][][][]int {
	schedule := make([][][][]int, kalle.settings.DayEndHour-kalle.settings.DayStartHour)

	for hm := 0; hm < (kalle.settings.DayEndHour - kalle.settings.DayStartHour); hm++ {
		schedule[hm] = make([][][]int, 60/resolution)

		for r := 0; r < 60/resolution; r++ {
			schedule[hm][r] = make([][]int, numDays)

			for day := 0; day < numDays; day++ {
				schedule[hm][r][day] = make([]int, 0, dayWidth)
				e := events.events[day][hm*60/resolution+r]

				usedWidth := 0

				slot := 0
				numFound := 0
				partWidth := 0
				for numFound < len(e) {
					partWidth = dayWidth / e[0].inParallel
					slotFound := false
					for i := range e {
						if e[i].slot == slot || e[i].slot == -1 {
							for j := 0; j < partWidth; j++ {
								schedule[hm][r][day] = append(schedule[hm][r][day], e[i].id)
							}
							numFound++
							slotFound = true
							break
						}
					}
					slot++
					if !slotFound {
						for i := 0; i < partWidth; i++ {
							schedule[hm][r][day] = append(schedule[hm][r][day], 0)
						}
					}
					usedWidth += partWidth
				}

				v := 0
				if len(e) > 0 && (dayWidth-usedWidth) < partWidth {
					v = schedule[hm][r][day][len(schedule[hm][r][day])-1]
				}
				for i := 0; i < (dayWidth - usedWidth); i++ {
					schedule[hm][r][day] = append(schedule[hm][r][day], v)
				}
			}
		}
	}
	return schedule
}

func (kalle *Kalle) todayView() {
	startDate := time.Date(kalle.now.Year(), kalle.now.Month(), kalle.now.Day(), 0, 0, 0, 0, kalle.now.Location())
	endDate := time.Date(kalle.now.Year(), kalle.now.Month(), kalle.now.Day(), 23, 59, 59, 0, kalle.now.Location())

	events := kalle.settings.getCellEvents(startDate, endDate, 1, 15)

	id2event := make(map[int]*Event)
	for i := range events.events {
		for j := range events.events[i] {
			for k := range events.events[i][j] {
				id2event[events.events[i][j][k].id] = events.events[i][j][k]
			}
		}
	}

	schedule := kalle.prepareForDisplay(events, 15, 1, kalle.width-20)

	titleShown := make(map[int]bool)
	locationShown := make(map[int]bool)

	for hm := 0; hm < len(schedule); hm++ {
		for r := 0; r < len(schedule[hm]); r++ {
			m := kalle.showTime(hm, r==0) + " "

			for day := 0; day < len(schedule[hm][r]); day++ {
				c := -1
				for i := 0; i < len(schedule[hm][r][day]);i++ {
					if c != schedule[hm][r][day][i] {
						c = schedule[hm][r][day][i]
						if c == 0 {
							m += textcolor.Color(textcolor.Black, kalle.colorTheme.freeBg[(startDate.Day()+day)%len(kalle.colorTheme.freeBg)])
						} else {
							m += textcolor.Color(textcolor.Black, kalle.colorTheme.occupied[(c-1)%len(kalle.colorTheme.occupied)])
						}
					}
					addText := func (text string) {
						l := 0
						for z := i; z < len(schedule[hm][r][day]) && schedule[hm][r][day][z] == c; z++ {
							l++
						}
						t := left(text, l)
						m += t
						i += len(t)
					}
					if _, shown := titleShown[c]; !shown && c > 0 {
						titleShown[c] = true
						addText(id2event[c].summary)
						continue
					}
					if _, shown := locationShown[c]; !shown && c > 0 {
						locationShown[c] = true
						addText(id2event[c].location)
						continue
					}
					if c == 0 {
						m += "▒"
					} else {
						m += " "
					}
				}
			}
			m += textcolor.Reset + " " + kalle.showTime(hm, r==0) + textcolor.Reset
			fmt.Println(m)
		}
	}
}

func (kalle *Kalle) commingDaysView() {
	kalle.showDaysView(kalle.now, 8, 0)
}

func (kalle *Kalle) weekView() {

	var startDate time.Time
	var todayOffset int

	if kalle.now.Weekday() == time.Sunday {
		todayOffset = 6
		startDate = kalle.now.AddDate(0, 0, -todayOffset)

	} else {
		todayOffset = int(kalle.now.Weekday()) - 1
		startDate = kalle.now.AddDate(0, 0, -todayOffset)
	}

	kalle.showDaysView(startDate, 7, todayOffset)
}

func (kalle *Kalle) showDaysView(startDate time.Time, numDays, todayOffset int) {

	startDate = time.Date(startDate.Year(), startDate.Month(), startDate.Day(), 0, 0, 0, 0, startDate.Location())
	endDate := startDate.AddDate(0, 0, numDays)
	endDate = time.Date(endDate.Year(), endDate.Month(), endDate.Day(), 23, 59, 59, 0, startDate.Location())

	resolution := 15
	events := kalle.settings.getCellEvents(startDate, endDate, numDays, resolution)

	dayWidth := (kalle.width - 10) / numDays

	// [hour][60/resolution][day]int
	// free = 0, other id

	schedule := kalle.prepareForDisplay(events, resolution, numDays, dayWidth)

	var dayOfWeek int
	if startDate.Weekday() == time.Sunday {
		dayOfWeek = 6
	} else {
		dayOfWeek = int(startDate.Weekday())-1
	}

	dayTitle := strings.Repeat(" ", 6)
	currentDate := startDate
	for i := dayOfWeek; i < numDays+dayOfWeek; i++ {
		day := fmt.Sprintf("%s %d", kalle.settings.DayNames[i % 7], currentDate.Day())
		if i != todayOffset+dayOfWeek {
			if i % 7 == 6 {
				dayTitle += textcolor.Color(kalle.colorTheme.holidayFg, kalle.colorTheme.daysBg)
			} else if i % 7 == 5 {
				dayTitle += textcolor.Color(kalle.colorTheme.saturdayFg, kalle.colorTheme.daysBg)
			} else {
				dayTitle += textcolor.Color(kalle.colorTheme.days, kalle.colorTheme.daysBg)
			}
		} else {
			dayTitle += textcolor.Color(kalle.colorTheme.today, kalle.colorTheme.todayBg)
		}
		dayTitle += center(day, dayWidth)
		// TODO: Layout is so so
		dayTitle = dayTitle[:len(dayTitle)-4] + moonPhase(currentDate) + " " + flagDay(currentDate)

		currentDate = currentDate.AddDate(0, 0, 1)
	}

	totalLen := dayWidth*numDays + 5

	title := kalle.settings.MonthNames[startDate.Month()-1]
	if startDate.Month() != endDate.Month() {
		title += "-" + kalle.settings.MonthNames[endDate.Month()-1]
	}

	title += fmt.Sprintf(" %d", startDate.Year())
	if startDate.Year() != endDate.Year() {
		title += fmt.Sprintf("-%d", endDate.Year())
	}

	_, weeknum := startDate.ISOWeek()
	title += fmt.Sprintf(", %s %d", kalle.settings.Week, weeknum)
	_, endweeknum := endDate.AddDate(0,0,-1).ISOWeek()
	if weeknum != endweeknum {
		title += fmt.Sprintf(" - %d", endweeknum)
	}

	fmt.Println("\n" + textcolor.Color(kalle.colorTheme.title, kalle.colorTheme.titleBg) + center(title, totalLen) + textcolor.Reset)
	fmt.Println() // Extra info about the days
	fmt.Println(dayTitle)

	for hm := 0; hm < len(schedule); hm++ {
		for r := 0; r < len(schedule[hm]); r += 2 {
			m := kalle.showTime(hm, r == 0) + " "

			for day := 0; day < len(schedule[hm][r]); day++ {
				actualDay := time.Weekday(int(startDate.Weekday()+time.Weekday(day)) % 7)
				upColor, dnColor := -1, -1
				char := " "

				for i := 0; i < len(schedule[hm][r][day]); i++ {

					if schedule[hm][r][day][i] == schedule[hm][r+1][day][i] {
						if upColor != schedule[hm][r][day][i] {
							upColor = schedule[hm][r][day][i]
							dnColor = upColor
							char = " "
						}
						if upColor == 0 {
							if actualDay == time.Saturday {
								m += textcolor.Color(textcolor.Black, kalle.colorTheme.saturdayBg)
							} else if actualDay == time.Sunday {
								m += textcolor.Color(textcolor.Black, kalle.colorTheme.holidayBg[1])
							} else {
								m += textcolor.Color(textcolor.Black, kalle.colorTheme.freeBg[(startDate.Day()+day)%len(kalle.colorTheme.freeBg)])
							}
						} else {
							m += textcolor.Color(textcolor.Black, kalle.colorTheme.occupied[(upColor-1)%len(kalle.colorTheme.occupied)])
						}

					} else {
						if upColor != schedule[hm][r][day][i] {
							upColor = schedule[hm][r][day][i]
							if upColor == 0 {
								if actualDay == time.Saturday {
									m += textcolor.Fg(kalle.colorTheme.saturdayBg)
								} else if actualDay == time.Sunday {
									m += textcolor.Fg(kalle.colorTheme.holidayBg[1])
								} else {
									m += textcolor.Fg(kalle.colorTheme.freeBg[(startDate.Day()+day)%len(kalle.colorTheme.freeBg)])
								}
							} else {
								m += textcolor.Fg(kalle.colorTheme.occupied[(upColor-1)%len(kalle.colorTheme.occupied)])
							}
							char = "▀"
						}
						if dnColor != schedule[hm][r+1][day][i] {
							dnColor = schedule[hm][r+1][day][i]
							//char = "▄"
							if dnColor == 0 {
								if actualDay == time.Saturday {
									m += textcolor.Bg(kalle.colorTheme.holidayBg[0])
								} else if actualDay == time.Sunday {
									m += textcolor.Bg(kalle.colorTheme.holidayBg[1])
								} else {
									m += textcolor.Bg(kalle.colorTheme.freeBg[(startDate.Day()+day)%len(kalle.colorTheme.freeBg)])
								}
							} else {
								m += textcolor.Bg(kalle.colorTheme.occupied[(dnColor-1)%len(kalle.colorTheme.occupied)])
							}
							char = "▀"
						}
					}
					m += char
				}
			}
			m += textcolor.Reset + " " + kalle.showTime(hm, r == 0) + textcolor.Reset
			fmt.Println(m)
		}
	}
	fmt.Println(textcolor.Reset)
}
