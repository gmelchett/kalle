package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/gmelchett/textcolor"
	"github.com/mitchellh/go-homedir"
	"github.com/peterh/liner"
)

const KALLE_SETTINGS = "~/.config/kalle/config.json"

type Settings struct {
	DayEndHour   int      `json:"dayEndHour"`
	DayNames     []string `json:"dayNames"`
	DayStartHour int      `json:"dayStartHour"`
	MonthNames   []string `json:"monthNames"`
	Week         string   `json:"week"`
	CalDav       struct {
		Root     string `json:"root"`
		User     string `json:"user"`
		Password string `json:"password"`
		Calendar string `json:"calendar"`
		FSPath   string `json:"fspath"`
	} `json:"CalDav"`
}

var defaultSettings = Settings{
	DayStartHour: 7,
	DayEndHour:   21,
	DayNames:     []string{"Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", "Lördag", "Söndag"},
	MonthNames:   []string{"Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"},
	Week:         "Vecka",
}

type ColorTheme struct {
	title, titleBg int
	days, daysBg   int
	today, todayBg int
	time, timeBg   int
	freeBg         []int
	occupied       []int
	wholeday       int
	saturdayBg     int
	saturdayFg     int
	holidayFg      int
	holidayBg      []int
	currentHour    int
}

var Ocean = ColorTheme{
	title:       49,
	days:        39,
	today:       100,
	todayBg:     45,
	time:        78,
	freeBg:      []int{25, 31},
	occupied:    []int{45, 81, 117},
	wholeday:    56,
	saturdayBg:  79,
	saturdayFg:  27,
	holidayBg:   []int{73, 85},
	holidayFg:   33,
	currentHour: 43,
}

type Kalle struct {
	colorTheme *ColorTheme
	settings   *Settings
	width      int
	now        time.Time
}

func loadSettings(settingsFile string) *Settings {

	fulldir, err := homedir.Expand(filepath.Dir(settingsFile))

	if err != nil {
		log.Printf("Failed to expand: %s\n", settingsFile)
		log.Fatalln(err)
	}
	fullpath := filepath.Join(fulldir, filepath.Base(settingsFile))

	if err != nil {
		log.Printf("Getting home directory for: %s failed\n", settingsFile)
		log.Fatalln(err)
	}

	if f, err := os.Open(fullpath); err == nil {
		defer f.Close()

		var s Settings

		if err := json.NewDecoder(f).Decode(&s); err != nil {
			log.Printf("%s is faulty.", settingsFile)
			log.Fatalln(err)
		}
		return &s
	}
	if jsonConfig, err := json.MarshalIndent(defaultSettings, "", "    "); err == nil {

		if stat, err := os.Stat(filepath.Dir(fullpath)); err != nil || !stat.IsDir() {
			err = os.MkdirAll(filepath.Dir(fullpath), 0755)
		}

		if f, err := os.OpenFile(fullpath, os.O_RDWR|os.O_CREATE, 0600); err == nil {
			f.Write(jsonConfig)
			f.Close()
			log.Printf("\"%s\" created. Please edit to suite your needs.\n", fullpath)
			os.Exit(1)
		} else {
			log.Printf("Failed to create settings file: %s\n", settingsFile)
			log.Fatalln(err)
		}
	}
	return &defaultSettings
}

func (s *Settings) check() {

	if s.DayStartHour < 0 || s.DayStartHour > 23 || s.DayEndHour < 0 || s.DayEndHour > 23 || s.DayEndHour <= s.DayStartHour {
		log.Fatalf("DayStartHour(%d) and DayEndHour(%d) must be between 0 and 23 and DayEndHour must be larger than DayStartHour.\n",
			s.DayStartHour, s.DayEndHour)
	}
	if len(s.DayNames) != 7 {
		log.Fatalf("Too many/few day names(%d)\n", len(s.DayNames))
	}

	// TODO? fspath presence takes precedence
	if s.CalDav.FSPath == "" {
		if len(s.CalDav.Root) == 0 {
			log.Fatalln("No caldav server root given.")
		}

		if len(s.CalDav.User) == 0 {
			log.Fatalln("No caldav user given.")
		}

		if len(s.CalDav.Password) == 0 {
			ln := liner.NewLiner()
			defer ln.Close()
			if pwd, err := ln.PasswordPrompt("Enter caldav password: "); err == nil {
				s.CalDav.Password = pwd
			} else {
				log.Println(err)
				log.Fatalln("Failed to get password.")
			}
		}
	}
}

func main() {

	kalle := Kalle{colorTheme: &Ocean, settings: loadSettings(KALLE_SETTINGS), width: 80, now: time.Now()}

	kalle.settings.check()

	if width, _, err := textcolor.GetSize(int(os.Stdin.Fd())); err == nil {
		kalle.width = width
	}

	var args = os.Args
	if len(args) > 1 {
		switch args[1] {
		case "today":
			kalle.todayView()
		case "next":
			kalle.nextView()
		case "week":
			kalle.weekView()
		default:
			fmt.Println("Unknown argument \"" + args[1] + "\"")
		}
	} else { // Default mode
		kalle.commingDaysView()
	}
}
