package main

import (
	"fmt"
	"time"
	"github.com/teambition/rrule-go"
)

type calenderCellEvents struct {
	events        [][][]*Event
	allDay [][]*Event
	maxInParallel int
}

type CalendarEvents struct {
	events []*Event
}

func (s *Settings) getSpannedEvents(startdate, enddate time.Time) []*Event {
	allEvents := s.fetchCalender()

	var events []*Event

	id := 1
	for i := range allEvents {
		if allEvents[i].starttime == nil || allEvents[i].endtime == nil {
			continue
		}
		st := *allEvents[i].starttime
		et := *allEvents[i].endtime

		if et.Before(startdate) || st.After(enddate) {
			continue
		}
		// Give the events we keep and unique id
		allEvents[i].id = id
		allEvents[i].slot = -1 // no slot allocated
		allEvents[i].inParallel = 1
		events = append(events, &allEvents[i])
		id++
	}

	return events
}

func (s *Settings) getCellEvents(startdate, enddate time.Time, numDays, resolution int) *calenderCellEvents {

	// [day in series 0 to numdays][time 0=starthourminute][*events]
	ce := calenderCellEvents{events: make([][][]*Event, numDays), allDay: make([][]*Event, numDays)}

	for i := 0; i < numDays; i++ {
		ce.events[i] = make([][]*Event, (s.DayEndHour-s.DayStartHour)*60/resolution)
		ce.allDay[i] = make([]*Event, 0)
	}

	allEvents := s.fetchCalender()

	var events []Event
	var i int

	id := 1

	addEvent := func(ss, ee *time.Time) {
		events = append(events, Event{id:id, slot:-1, inParallel:1, summary: allEvents[i].summary,
			location: allEvents[i].location, starttime: ss, endtime: ee})
		id++
	}

	// Filter out events that are outside the current request
	for i = range allEvents {
		/* Start and end time is set for eternal rrules as well */
		if allEvents[i].starttime == nil || allEvents[i].endtime == nil {
			continue
		}
		st := *allEvents[i].starttime
		et := *allEvents[i].endtime

		if len(allEvents[i].rrule) > 0 {
			if r, err := rrule.StrToRRule(allEvents[i].rrule); err == nil {
				for _, t := range r.Between(startdate, enddate.AddDate(0,0,-1), true) {
					// TODO: handle repeated whole day events!
					tstart := time.Date(t.Year(), t.Month(), t.Day(), st.Hour(), st.Minute(), 0, 0, t.Location())
					tend := time.Date(t.Year(), t.Month(), t.Day(), et.Hour(), et.Minute(), 0, 0, t.Location())
					addEvent(&tstart, &tend)
				}
				continue
			} else {
				fmt.Println("ERROR: rrule-go can't decode: %s - ignoring this event!\n", allEvents[i].rrule)
				continue
			}
		}

		if et.Before(startdate) || st.After(enddate) {
			continue
		}

		if allEvents[i].allDay {
			d := int(startdate.Sub(st).Hours()/24)
			e := Event{id:id, slot:-1, inParallel:1, summary: allEvents[i].summary,
				location: allEvents[i].location}
			id++

			ce.allDay[d] = append(ce.allDay[d], &e)

			continue
		}

		duration := et.Sub(st)

		if duration.Hours() >= 24 {
			// Handle several days long event
			for duration.Hours() >= 24 {

				if st.Hour() == 0 && st.Minute() == 0 {
					d := int(startdate.Sub(st).Hours()/24)
					e := Event{id:id, slot:-1, inParallel:1, summary: allEvents[i].summary,
						location: allEvents[i].location}
					id++

					ce.allDay[d] = append(ce.allDay[d], &e)
				} else {
					et1 := time.Date(st.Year(), st.Month(), st.Day(), 23, 59, 59, 0, st.Location())
					addEvent(&st, &et1)
				}
				tmp := st.AddDate(0,0,1)

				st = time.Date(tmp.Year(), tmp.Month(), tmp.Day(), 0,0,0,0, tmp.Location())
				duration = et.Sub(st)
			}
			if duration.Minutes() > 0 {
				addEvent(&st, &et)
			}
			continue
		}

		// Give the events we keep and unique id
		allEvents[i].id = id
		allEvents[i].slot = -1 // no slot allocated
		allEvents[i].inParallel = 1
		events = append(events, allEvents[i])
		id++
	}

	for now := startdate; now.Before(enddate); now = now.AddDate(0, 0, 1) {
		for i := range events {

			st := *events[i].starttime
			et := *events[i].endtime

			if st.Day() != now.Day() && et.Day() != now.Day() {
				continue
			}

			if events[i].allDay {
				continue
			}

			// Delta from actual start, rounded to resolution minutes
			var deltamin int
			if st.Minute()%resolution < (resolution / 2) {
				deltamin = -st.Minute() % resolution
			} else {
				deltamin = resolution - st.Minute()%resolution
			}

			st = st.Add(time.Duration(deltamin) * time.Minute)

			if et.Minute()%resolution < (resolution / 2) {
				deltamin = -et.Minute() % resolution
			} else {
				deltamin = resolution - et.Minute()%resolution
			}
			et = et.Add(time.Duration(deltamin) * time.Minute)

			duration := et.Sub(st)

			if duration.Minutes() == 0 {
				continue
			}

			startHM := st.Hour()*60 + st.Minute()

			for d := 0; d < int(duration.Minutes()); d += resolution {
				if (startHM+d)%(24*60) < s.DayStartHour*60 {
					continue
				}
				if (startHM+d)%(24*60) >= s.DayEndHour*60 {
					continue
				}
				didx := st.Sub(startdate)/time.Hour/24
				if startHM+d >= 24*60 {
					didx++
				}
				midx := (startHM + d - s.DayStartHour*60) % (24 * 60) / resolution

				if len(ce.events[didx][midx]) > 0 {
					e := ce.events[didx][midx]
					if len(e) == 1 && e[0].slot < 0 {
						ce.events[didx][midx][0].slot = 0
					}
					if events[i].slot < 0 {
						freeSlotFound := false
						freeSlot := 0
						for !freeSlotFound {
							freeSlotFound = true
							for k := range e {
								if e[k].slot == freeSlot {
									freeSlot++
									freeSlotFound = false
									break
								}
							}
						}
						events[i].slot = freeSlot
					}
					ce.events[didx][midx] = append(ce.events[didx][midx], &events[i])
					for k := range ce.events[didx][midx] {
						ce.events[didx][midx][k].inParallel = len(ce.events[didx][midx])
						if ce.events[didx][midx][k].inParallel > ce.maxInParallel {
							ce.maxInParallel = ce.events[didx][midx][k].inParallel
						}
					}
				} else {
					ce.events[didx][midx] = append([]*Event{}, &events[i])
				}
			}
		}
	}
	return &ce
}
