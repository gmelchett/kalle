package main

import (
	"bitbucket.org/gmelchett/textcolor"
	"math"
	"time"
)

func moonPhase(when time.Time) string {

	if when.Year() < 2019 {
		return " "
	}

	yearday := float64(when.YearDay())

	for y := 2019; y < when.Year(); y++ {
		yearday += 365.25
	}

	/* New moon 6th January 2019, full moon 21st January 2019 */
	for yearday > 30 {
		yearday -= 29.53

	}

	if math.Round(yearday) == 6 {
		return textcolor.Fg(textcolor.Black) + "\u25CF"
	} else if math.Round(yearday) == 21 {
		return textcolor.Fg(textcolor.White) + "\u25CF"
	} else {
		return " "
	}
}

type FlagDay struct {
	month, day int
}

func flagDaySweden(when time.Time) string {

	days := []FlagDay{
		{1, 1},
		{1, 28},
		{3, 12},
		/* Easterday */
		{4, 30},
		{5, 1},
		/* Pingst */
		{5, 29},
		{6, 6},
		/* Midsummer's eve */
		{7, 14},
		{8, 8},
		/* election day */
		{10, 24},
		{11, 6},
		{12, 10},
		{12, 23},
		{12, 25}}

	for i := range days {
		if days[i].month == int(when.Month()) && days[i].day == when.Day() {
			return textcolor.Color(textcolor.Yellow, textcolor.Blue) + "\u254B\u2501"
		}
	}
	return "  "
}

func flagDay(when time.Time) string {
	return flagDaySweden(when)
}
