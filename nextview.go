package main

import (
	"fmt"
)

func (kalle *Kalle) nextView() {
	events := kalle.settings.getSpannedEvents(kalle.now, kalle.now.AddDate(0, 0, 7))

	fmt.Println("Next appointments")
	fmt.Println("=================")

	for i := range events {
		summary := events[i].summary
		starttime := events[i].starttime
		endtime := events[i].endtime

		fmt.Printf("* %-32v - %-32v : %v\n", starttime, endtime, summary)
	}
}
