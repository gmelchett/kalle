package main

import (
	"fmt"
	"time"
	ics "github.com/arran4/golang-ical"
	"strconv"
	"regexp"
)

type Event struct {
	summary    string
	location   string
	rrule      string
	starttime  *time.Time
	endtime    *time.Time
	allDay     bool
	inParallel int
	slot       int
	id         int
}

const icalTimeFormat = "20060102T150405"
const icalDateFormat = "20060102"

func parseDateTime(p *ics.IANAProperty) (*time.Time, bool) {
	var locerr error
	var loc *time.Location

	if t, exists := p.ICalParameters["TZID"]; exists && len(t) == 1 {
		loc, locerr = time.LoadLocation(t[0])
	}

	if locerr == nil {
		if t, err := time.ParseInLocation(icalTimeFormat, p.Value, loc); err == nil {
			return &t, false
		}
	} else {
		if t, err := time.Parse(icalTimeFormat, p.Value); err == nil {
			return &t, false
		}
	}

	// Date does not have a time zone
	if t, err := time.Parse(icalDateFormat, p.Value); err == nil {
		return &t, true
	}
	return nil, false
}

// Copied from: https://github.com/laurent22/ical-go/
func parseDuration(durStr string) time.Duration {

	if durStr == "" {
		return time.Duration(0)
	}

	durRgx := regexp.MustCompile("PT(?:([0-9]+)H)?(?:([0-9]+)M)?(?:([0-9]+)S)?")
	matches := durRgx.FindStringSubmatch(durStr)

	if len(matches) != 4 {
		return time.Duration(0)
	}

	strToDuration := func(value string) time.Duration {
		d := 0
		if value != "" {
			d, _ = strconv.Atoi(value)
		}
		return time.Duration(d)
	}

	hours := strToDuration(matches[1])
	min := strToDuration(matches[2])
	sec := strToDuration(matches[3])

	return hours*time.Hour + min*time.Minute + sec*time.Second
}

func (s *Settings) fetchCalender() []Event {
	raw := []Event{}

	src := NewSource(s)
	files, err := src.ListFiles()
	if err != nil {
		fmt.Printf("reading from source (type %T) failed: %s\n", src, err)
		return raw
	}

	for _, file := range files {
		d, err := src.Open(file)
		if err != nil {
			fmt.Println(err)
			continue
		}

		cal, err := ics.ParseCalendar(d)
		if err != nil {
			src.Close(file)
			fmt.Printf("Faulty calendar: %s continuing\n", file)
			continue
		}

		src.Close(file)

		for _, e := range cal.Events() {

			var this Event
			fmt.Println(e)

			if p := e.GetProperty("RRULE"); p != nil {
				this.rrule = p.Value
			}

			if p := e.GetProperty(ics.ComponentPropertyDtStart); p != nil {
				this.starttime, this.allDay = parseDateTime(p)
			}
			if p := e.GetProperty(ics.ComponentPropertyDtEnd); p != nil {
				this.endtime, this.allDay = parseDateTime(p)
			}

			if this.endtime == nil {
				if p := e.GetProperty("DURATION"); p != nil {
					t := (*this.starttime).Add(parseDuration(p.Value))
					this.endtime = &t
				}
			}

			fmt.Println(*this.starttime, *this.endtime)

			if p := e.GetProperty(ics.ComponentPropertySummary); p != nil {
				this.summary = p.Value
			}
			if p := e.GetProperty(ics.ComponentPropertyLocation); p != nil {
				this.location = p.Value
			}

			if this.starttime != nil {
				fmt.Println(*this.starttime)
			} else {
				fmt.Println(this.summary, "No start time")
			}
			if this.endtime != nil {
				fmt.Println(*this.endtime)
			} else {
				fmt.Println(this.summary, "No end time")
			}

			raw = append(raw, this)
		}
	}
	return raw
}
